package com.danilodequeiroz.itauchallenge

import android.os.Build
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.itauchallenge.app.CustomApplication
import com.danilodequeiroz.itauchallenge.common.launchActivity
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.ContactTransferActivity
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun appContext_verify_AppName() {
        val appContext = getApplicationContext<CustomApplication>()
        assertEquals("com.danilodequeiroz.itauchallenge", appContext.packageName)
    }
}

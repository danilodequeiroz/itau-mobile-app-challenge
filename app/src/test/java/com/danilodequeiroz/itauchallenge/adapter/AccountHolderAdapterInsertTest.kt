package com.danilodequeiroz.itauchallenge.adapter

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter.AccountHolderContactsAdapter
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import android.service.quicksettings.Tile
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class AccountHolderAdapterInsertTest {

    lateinit var adapter: AccountHolderContactsAdapter

    @Before
    @Throws(Exception::class)
    fun setUp() {
        adapter = AccountHolderContactsAdapter(mutableListOf())
        MockitoAnnotations.initMocks(this);
    }


    @Test
    fun single_item_adding_testCount() {
        assertThat(adapter.itemCount).isEqualTo(adapter.accountHolderContacts.size)
        adapter.addItem(AccountHolderContactViewModel("Contatinho"))
        assertThat(adapter.itemCount).isEqualTo(1)
    }

    @Test
    fun multiple_items_adding_testCount() {
        assertThat(adapter.itemCount).isEqualTo(adapter.accountHolderContacts.size)
        adapter.putAllItems(
            mutableListOf(
                AccountHolderContactViewModel("Contatinho 1"),
                AccountHolderContactViewModel("Contatinho 2"),
                AccountHolderContactViewModel("Contatinho 3"),
                AccountHolderContactViewModel("Contatinho 4")
            )
        )
        assertThat(adapter.itemCount).isEqualTo(4)
    }

    @Test
    fun multiple_items_adding_testCount_() {
        assertThat(adapter.itemCount).isEqualTo(adapter.accountHolderContacts.size)
        adapter.putAllItems(
            mutableListOf(
                AccountHolderContactViewModel("Contatinho 1"),
                AccountHolderContactViewModel("Contatinho 2"),
                AccountHolderContactViewModel("Contatinho 3"),
                AccountHolderContactViewModel("Contatinho 4")
            )
        )
        assertThat(adapter.itemCount).isEqualTo(4)
    }

    @Test
    fun spy_single_item_insert_testInternalNotify() {
        adapter = spy(AccountHolderContactsAdapter(mutableListOf()))

        doNothing().`when`(adapter).notifyItemInserted(anyInt())

        adapter.addItem(AccountHolderContactViewModel("Contatinho 1"))

        verify(adapter).notifyItemInserted(0)
    }

    @Test
    fun spy_multiple_item_insert_testInternalNotify() {
        val spyAdapter = spy(AccountHolderContactsAdapter(mutableListOf()))

        doNothing().`when`(spyAdapter).notifyItemInserted(anyInt())

        spyAdapter.putAllItems(
            mutableListOf(
                AccountHolderContactViewModel("Contatinho 1"),
                AccountHolderContactViewModel("Contatinho 2"),
                AccountHolderContactViewModel("Contatinho 3"),
                AccountHolderContactViewModel("Contatinho 4")
            )
        )

        verify(spyAdapter).notifyDataSetChanged()
    }
}
package com.danilodequeiroz.itauchallenge.adapter

import android.os.SystemClock
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.common.launchActivity
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.ContactTransferActivity
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.not
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.Robolectric.buildActivity
import org.robolectric.Shadows


@RunWith(AndroidJUnit4::class)
class AccountHolderAdapterHandlingTest {


    @Test
    fun click_on_top_select_item_checkSelectedOnUi() {
        val scenario = launchActivity<ContactTransferActivity>()
        val controller = buildActivity<ContactTransferActivity>(ContactTransferActivity::class.java).setup()
        controller.get().findViewById<LinearLayout>(R.id.llSelectContacts).performClick()
        assert(controller.get().findViewById<RecyclerView>(R.id.rvSelectContacts).visibility == View.VISIBLE)

        Robolectric.getForegroundThreadScheduler().pause()
        controller.get().findViewById<RecyclerView>(R.id.rvSelectContacts).findViewHolderForAdapterPosition(0)?.itemView?.performClick()
        Robolectric.getForegroundThreadScheduler().unPause()

        assert(controller.get().findViewById<RecyclerView>(R.id.rvSelectContacts).visibility == View.GONE)
        assert(controller.get().findViewById<TextView>(R.id.tvContactHeader).text == "Alessandro Venturella")
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }
}
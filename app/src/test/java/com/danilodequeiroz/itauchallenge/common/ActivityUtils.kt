package com.danilodequeiroz.itauchallenge.common

import android.app.Activity
import androidx.test.core.app.ActivityScenario

inline fun <reified T:Activity> launchActivity()= ActivityScenario.launch(T::class.java)
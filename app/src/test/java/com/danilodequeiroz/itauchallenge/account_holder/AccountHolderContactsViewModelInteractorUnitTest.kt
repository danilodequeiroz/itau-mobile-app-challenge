package com.danilodequeiroz.itauchallenge.account_holder

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.common.mock
import com.danilodequeiroz.itauchallenge.common.presentation.*
import com.danilodequeiroz.itauchallenge.common.whenever
import com.danilodequeiroz.itauchallenge.contact_transfer.domain.AccountContactTransferInteractor
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.screen.ContactTransferViewModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.verify
import java.net.SocketException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException


class AccountHolderContactsViewModelInteractorUnitTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    val interactor = mock<AccountContactTransferInteractor>()
    val observerState = mock<Observer<ContactsState>>()

    val viewmodel by lazy { ContactTransferViewModel(interactor, Schedulers.trampoline(), Schedulers.trampoline()) }

    @Before
    fun initTest() {
        Mockito.reset(interactor, observerState)
    }

    @Test
    fun test_FetchOnce_AccountHolderContacts_Default_State() {
        val givenResponse = mutableListOf(AccountHolderContactViewModel())
        whenever(interactor.getAccountHolderContacts())
            .thenReturn(Single.just(givenResponse))

        viewmodel.stateAccountHolderContacts.observeForever(observerState)
        viewmodel.initAccountHolderContacts()

        verify(interactor).getAccountHolderContacts()

        val argumentCaptor = ArgumentCaptor.forClass(ContactsState::class.java)
        val expectedInitialState = DefaultContactsState(mutableListOf())
        val expectedLoadingState = LoadingContactsState(mutableListOf())
        val expectedDefaultState = DefaultContactsState(givenResponse)

        argumentCaptor.run {
            verify(observerState, Mockito.times(3)).onChanged(this.capture())

            val (initialState, loadingState, defaultState) = allValues

            assertEquals(initialState, expectedInitialState)
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(defaultState, expectedDefaultState)
        }
    }

    @Test
    fun test_FetchOnce_AccountHolderContacts_Error_State() {
        val errorMessage = "Error response"
        val response = Throwable(errorMessage)
        whenever(interactor.getAccountHolderContacts())
            .thenReturn(Single.error(response))

        viewmodel.stateAccountHolderContacts.observeForever(observerState)
        viewmodel.initAccountHolderContacts()

        verify(interactor).getAccountHolderContacts()

        val argumentCaptor = ArgumentCaptor.forClass(ContactsState::class.java)
        val expectedInitialState = DefaultContactsState(mutableListOf())
        val expectedLoadingState = LoadingContactsState(mutableListOf())
        val expectedErrorState = ErrorContactsState(R.string.technical_error_message, mutableListOf())

        argumentCaptor.run {
            verify(observerState, Mockito.times(3)).onChanged(capture())

            val (initialState, loadingState, errorState) = allValues

            assertEquals(initialState, expectedInitialState)
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(errorState, expectedErrorState)
        }
    }

    @Test
    fun test_FetchOnce_AccountHolderContacts_NetworkError_State() {
        val errorMessage = "Got an Network error"
        val response = listOf(
            TimeoutException(errorMessage),
            SocketException(errorMessage),
            UnknownHostException(errorMessage)
        ).random()


        whenever(interactor.getAccountHolderContacts())
            .thenReturn(Single.error(response))

        viewmodel.stateAccountHolderContacts.observeForever(observerState)
        viewmodel.initAccountHolderContacts()

        verify(interactor).getAccountHolderContacts()

        val argumentCaptor = ArgumentCaptor.forClass(ContactsState::class.java)
        val expectedInitialState = DefaultContactsState(mutableListOf())
        val expectedLoadingState = LoadingContactsState(mutableListOf())
        val expectedErrorState = NetworkErrorContactsState(R.string.network_error_message, mutableListOf())

        argumentCaptor.run {
            verify(observerState, Mockito.times(3)).onChanged(capture())

            val (initialState, loadingState, errorState) = allValues

            assertEquals(initialState, expectedInitialState)
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(errorState, expectedErrorState)
        }
    }
}
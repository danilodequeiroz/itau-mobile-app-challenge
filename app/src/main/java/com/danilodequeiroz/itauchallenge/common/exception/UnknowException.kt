package com.danilodequeiroz.itauchallenge.common.exception

import java.io.IOException

class UnknowException : IOException("Erro desconhecido")
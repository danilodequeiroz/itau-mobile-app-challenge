package com.danilodequeiroz.itauchallenge.common.presentation

import androidx.annotation.StringRes
import com.danilodequeiroz.itauchallenge.viewmodel.BaseViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel



sealed class AccountBalanceState { abstract val data: MutableList<AccountBalanceViewModel> }

data class DefaultAccountBalanceState(override val data: MutableList<AccountBalanceViewModel>) : AccountBalanceState()
data class LoadingAccountBalanceState(override val data: MutableList<AccountBalanceViewModel>) : AccountBalanceState()
data class ErrorAccountBalanceState(@StringRes val errorMessage: Int, override val data: MutableList<AccountBalanceViewModel>) : AccountBalanceState()
data class NetworkErrorAccountBalanceState(@StringRes val errorMessage: Int, override val data: MutableList<AccountBalanceViewModel>) : AccountBalanceState()

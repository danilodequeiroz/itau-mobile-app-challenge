package com.danilodequeiroz.itauchallenge.common.exception

import java.io.IOException

class NoConnectivityException(message: String = "Sem conexão com a internet") : IOException(message)
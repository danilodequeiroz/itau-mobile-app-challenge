package com.danilodequeiroz.itauchallenge.common.presentation

import androidx.annotation.StringRes
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel


sealed class ContactsState { abstract val data: MutableList<AccountHolderContactViewModel> }

data class DefaultContactsState(override val data: MutableList<AccountHolderContactViewModel>) : ContactsState()
data class LoadingContactsState(override val data: MutableList<AccountHolderContactViewModel>) : ContactsState()
data class ErrorContactsState(@StringRes val errorMessage: Int, override val data: MutableList<AccountHolderContactViewModel>) : ContactsState()
data class NetworkErrorContactsState(@StringRes val errorMessage: Int, override val data: MutableList<AccountHolderContactViewModel>) : ContactsState()

package com.danilodequeiroz.itauchallenge.common.presentation

import androidx.annotation.StringRes
import com.danilodequeiroz.restapi.model.TransferOperationStatus


sealed class ContactsTransferState { abstract val data: TransferOperationStatus? }

data class DefaultContactsTransferState(override val data: TransferOperationStatus?) : ContactsTransferState()
data class LoadingContactsTransferState(override val data: TransferOperationStatus?) : ContactsTransferState()
data class ErrorContactsTransferState(@StringRes val errorMessage: Int, override val data: TransferOperationStatus?) : ContactsTransferState()
data class NetworkErrorContactsTransferState(@StringRes val errorMessage: Int, override val data: TransferOperationStatus?) : ContactsTransferState()
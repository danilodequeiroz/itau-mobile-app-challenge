package com.danilodequeiroz.itauchallenge.viewmodel.screen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.common.presentation.*
import com.danilodequeiroz.itauchallenge.contact_transfer.domain.AccountContactTransferInteractor
import com.danilodequeiroz.itauchallenge.di.SCHEDULER_IO
import com.danilodequeiroz.itauchallenge.di.SCHEDULER_MAIN_THREAD
import com.danilodequeiroz.itauchallenge.ext.isNetworkError
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.danilodequeiroz.restapi.model.OperationStatus
import com.danilodequeiroz.restapi.model.TransferOperationStatus
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

class ContactTransferViewModel @Inject constructor(
    val accountContactTransferInteractor: AccountContactTransferInteractor,
    @Named(SCHEDULER_IO) val subscribeOnScheduler: Scheduler,
    @Named(SCHEDULER_MAIN_THREAD) val observeOnScheduler: Scheduler
) : ViewModel() {

    val stateAccountTypesBalances = MutableLiveData<AccountBalanceState>()
    val stateAccountHolderContacts = MutableLiveData<ContactsState>()
    val stateOperationSuccessStatus = MutableLiveData<ContactsTransferState>()
    val moneyValue = MutableLiveData<String>()

    val disposable = CompositeDisposable()

    init {
        stateAccountTypesBalances.value = DefaultAccountBalanceState(mutableListOf())
        stateAccountHolderContacts.value = DefaultContactsState(mutableListOf())
        stateOperationSuccessStatus.value = DefaultContactsTransferState(null)
        moneyValue.value = "R$ 0,00"
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun initAccountBalances() {
        stateAccountTypesBalances.value =
            LoadingAccountBalanceState(mutableListOf())
        getAccountTypesBalances()
    }

    fun initAccountHolderContacts() {
        stateAccountHolderContacts.value =
            LoadingContactsState(mutableListOf())
        getAccountHolderContacts()
    }

    fun postAccountHolderContactTransfer() {
        disposable.add(
            accountContactTransferInteractor.postAccountHolderContactTransfer()
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .subscribe({ onPostAccountHolderContactTransfer(it) }, { t ->
                    stateOperationSuccessStatus.value = if (t.isNetworkError()) {
                        NetworkErrorContactsTransferState(R.string.network_error_message, TransferOperationStatus(OperationStatus.FALLBACK_ERROR))
                    } else {
                        ErrorContactsTransferState(R.string.technical_error_message, TransferOperationStatus(OperationStatus.FALLBACK_ERROR))
                    }
                })

        )
    }

    private fun onPostAccountHolderContactTransfer(it: TransferOperationStatus) {
        stateOperationSuccessStatus.value = DefaultContactsTransferState(it)
    }

    private fun getAccountTypesBalances() {
        disposable.add(
            accountContactTransferInteractor.getAccountTypesBalances()
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .subscribe({ onGetAccountTypesBalances(it) }, { t ->
                    stateAccountTypesBalances.value = if (t.isNetworkError()) {
                        NetworkErrorAccountBalanceState(R.string.network_error_message, mutableListOf())
                    } else {
                        ErrorAccountBalanceState(R.string.technical_error_message, mutableListOf())
                    }
                })

        )
    }

    private fun getAccountHolderContacts() {
        disposable.add(
            accountContactTransferInteractor.getAccountHolderContacts()
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .subscribe({ onGetAccountHolderContacs(it) }, { t ->
                    stateAccountHolderContacts.value = if (t.isNetworkError()) {
                        NetworkErrorContactsState(R.string.network_error_message, mutableListOf())
                    } else {
                        ErrorContactsState(R.string.technical_error_message, mutableListOf())
                    }
                })

        )
    }

    private fun onGetAccountTypesBalances(accBalanceList: MutableList<AccountBalanceViewModel>) {
        val defaultAccountBalanceState = DefaultAccountBalanceState(accBalanceList)
        stateAccountTypesBalances.value = defaultAccountBalanceState
    }

    private fun onGetAccountHolderContacs(accBalanceList: MutableList<AccountHolderContactViewModel>) {
        val defaultContactsState = DefaultContactsState(accBalanceList)
        stateAccountHolderContacts.value = defaultContactsState
    }

    fun restore() {
        stateAccountTypesBalances.value =
            DefaultAccountBalanceState(stateAccountTypesBalances.value?.data?.toMutableList() ?: mutableListOf())
        stateAccountHolderContacts.value =
            DefaultContactsState(stateAccountHolderContacts.value?.data?.toMutableList() ?: mutableListOf())
        stateOperationSuccessStatus.value =
            DefaultContactsTransferState(stateOperationSuccessStatus.value?.data)
        moneyValue.value = moneyValue.value
    }


}

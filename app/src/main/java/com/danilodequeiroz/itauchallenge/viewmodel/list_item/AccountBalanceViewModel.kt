package com.danilodequeiroz.itauchallenge.viewmodel.list_item

import android.os.Parcelable
import com.danilodequeiroz.itauchallenge.viewmodel.BaseViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountBalanceViewModel(

    val accountTypeName: String? = null,
    val balanceFormatted: String? = null

) : BaseViewModel(), Parcelable
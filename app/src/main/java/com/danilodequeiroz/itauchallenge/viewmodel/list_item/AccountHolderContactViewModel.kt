package com.danilodequeiroz.itauchallenge.viewmodel.list_item

import android.os.Parcelable
import com.danilodequeiroz.itauchallenge.viewmodel.BaseViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountHolderContactViewModel(

    val name: String? = null

) : BaseViewModel(), Parcelable

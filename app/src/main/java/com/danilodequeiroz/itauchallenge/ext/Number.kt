package com.danilodequeiroz.itauchallenge.ext

import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.restapi.model.AccountBalance
import com.danilodequeiroz.restapi.model.AccountType
import java.util.*

fun Number.toBRLMoneyString(): String {
    return "R$ %.2f".format(Locale("pt", "BR"), this.toFloat())
}

fun AccountBalance.accountName(): Int {
    return when (this.accountType) {
        AccountType.SAVING_ACCOUNT -> R.string.saving_account
        AccountType.TRANSACTION_ACCOUNT -> R.string.transaction_account
    }
}
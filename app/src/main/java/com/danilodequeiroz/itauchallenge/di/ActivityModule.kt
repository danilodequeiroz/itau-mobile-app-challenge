package com.danilodequeiroz.itauchallenge.di

import com.danilodequeiroz.itauchallenge.contact_transfer.ui.ContactTransferActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeContactTransferActivity(): ContactTransferActivity
}
package com.danilodequeiroz.itauchallenge.di

import com.danilodequeiroz.restapi.repository.AccountTransferRestRepository
import com.danilodequeiroz.restapi.repository.mock.AccountTransferMockRepositoryImpl
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

const val SCHEDULER_IO = "SCHEDULER_IO"
const val SCHEDULER_MAIN_THREAD = "SCHEDULER_MAIN_THREAD"
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(): AccountTransferRestRepository = AccountTransferMockRepositoryImpl()

    @Provides
    @Named(SCHEDULER_IO)
    fun provideObserveOnScheduler() = Schedulers.io()

    @Provides
    @Named(SCHEDULER_MAIN_THREAD)
    fun provideSubscribeOnScheduler() = AndroidSchedulers.mainThread()

}
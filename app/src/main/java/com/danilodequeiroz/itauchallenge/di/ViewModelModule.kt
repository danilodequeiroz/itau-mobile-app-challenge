package com.danilodequeiroz.itauchallenge.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.danilodequeiroz.itauchallenge.di.annotation.ViewModelKey
import com.danilodequeiroz.itauchallenge.di.manufacturer.ViewModelFactory
import com.danilodequeiroz.itauchallenge.viewmodel.screen.ContactTransferViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ContactTransferViewModel::class)
    protected abstract fun bindForgotPasswordViewModel(contactTransferViewModel: ContactTransferViewModel): ViewModel

}
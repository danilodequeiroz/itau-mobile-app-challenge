package com.danilodequeiroz.itauchallenge.contact_transfer.domain

import android.app.Application
import com.danilodequeiroz.itauchallenge.common.MoneyUtils
import com.danilodequeiroz.itauchallenge.ext.accountName
import com.danilodequeiroz.itauchallenge.ext.toBRLMoneyString
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.danilodequeiroz.restapi.repository.AccountTransferRestRepository
import javax.inject.Inject

class AccountContactTransferInteractor @Inject constructor(
    val repository: AccountTransferRestRepository,
    val application: Application
) :
    AccountTypesBalancesUseCase,
    AccountHolderContactsUseCase,
    AccountHolderContactTransferUseCase {


    override fun getAccountTypesBalances() = repository
        .getAccountTypesBalances()
        .map { list ->
            list.map { accHolder ->
                AccountBalanceViewModel(
                    application.getString(accHolder.accountName()),
                    MoneyUtils.formattedValue(accHolder.balance)
                )
            }.toMutableList()
        }

    override fun postAccountHolderContactTransfer() = repository
        .postAccountHolderContactTransfer()


    override fun getAccountHolderContacts() = repository
        .getAccountHolderContacts()
        .map { list ->
            list.map { accHolder ->
                AccountHolderContactViewModel(accHolder.contactName)
            }.toMutableList()
        }
}





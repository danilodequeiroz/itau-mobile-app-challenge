package com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import kotlinx.android.synthetic.main.list_child_item_for_expandable_minimal.view.*

class AccountBalancesAdapter(var accountsBalances: MutableList<AccountBalanceViewModel>) :
    RecyclerView.Adapter<AccountBalancesAdapter.MyChildViewHolder>() {

    var selectedItem: Int = -1
    var itemClickListener: OnItemClickListener<AccountBalanceViewModel>? = null

    override fun onBindViewHolder(holder: MyChildViewHolder, position: Int) {
        val accountsBalances = accountsBalances[position]
        holder.textView.text =
            String.format("%s (%s)", accountsBalances.accountTypeName, accountsBalances.balanceFormatted)
        holder.itemClickListener = itemClickListener
        holder.itemView.isSelected = position == selectedItem
        holder.markAsChecked(position == selectedItem)
    }

    override fun getItemCount() = accountsBalances.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyChildViewHolder =
        MyChildViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_child_item_for_expandable_minimal,
                parent,
                false
            )
        )


    fun clear() {
        selectedItem = -1
        accountsBalances.clear()
    }


    fun addItem(model: AccountBalanceViewModel) {
        accountsBalances.add(model)
        notifyItemInserted(accountsBalances.size - 1)
    }

    fun putAllItems(model: MutableList<AccountBalanceViewModel>) {
        accountsBalances.addAll(model)
        notifyDataSetChanged()
    }

    inner class MyChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                notifyItemChanged(selectedItem)
                selectedItem = layoutPosition
                notifyItemChanged(selectedItem)
                itemClickListener?.onItemClick(accountsBalances[selectedItem])
            }
        }

        var textView: TextView = itemView.tvChildItemTitle
        var imageCheck: ImageView = itemView.ivChecked
        var itemClickListener: OnItemClickListener<AccountBalanceViewModel>? = null

        fun markAsChecked(check: Boolean) {
            imageCheck.visibility = if (check) View.VISIBLE else View.GONE
        }
    }
}



package com.danilodequeiroz.itauchallenge.contact_transfer.domain

import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.danilodequeiroz.restapi.model.TransferOperationStatus
import io.reactivex.Single

interface AccountHolderContactsUseCase {
    fun getAccountHolderContacts() : Single<MutableList<AccountHolderContactViewModel>>
}

interface AccountTypesBalancesUseCase {
    fun getAccountTypesBalances() : Single<MutableList<AccountBalanceViewModel>>
}

interface AccountHolderContactTransferUseCase {
    fun postAccountHolderContactTransfer() : Single<TransferOperationStatus>
}



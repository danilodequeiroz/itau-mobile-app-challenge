package com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter

interface OnItemClickListener<T> {
    fun onItemClick(model: T)
}
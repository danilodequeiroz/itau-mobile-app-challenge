package com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import kotlinx.android.synthetic.main.list_child_item_for_expandable_minimal.view.*


class AccountHolderContactsAdapter(var accountHolderContacts: MutableList<AccountHolderContactViewModel>) :
    RecyclerView.Adapter<AccountHolderContactsAdapter.MyChildViewHolder>() {

    var selectedItem: Int = -1
    var itemClickListener: OnItemClickListener<AccountHolderContactViewModel>? = null

    override fun onBindViewHolder(holder: MyChildViewHolder, position: Int) {
        val contact = accountHolderContacts[position]
        holder.textView.text = contact.name
        holder.itemClickListener = itemClickListener
        holder.itemView.isSelected = position == selectedItem
        holder.markAsChecked(position == selectedItem)
    }

    override fun getItemCount() = accountHolderContacts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyChildViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.list_child_item_for_expandable_minimal,
            parent,
            false
        )
    )

    fun clear() {
        selectedItem = -1
        accountHolderContacts.clear()
    }

    fun addItem(model: AccountHolderContactViewModel) {
        accountHolderContacts.add(model)
        notifyItemInserted(accountHolderContacts.size - 1)
    }

    fun putAllItems(model: MutableList<AccountHolderContactViewModel>) {
        sortAlphabetically(model)
        accountHolderContacts.addAll(model)
        notifyDataSetChanged()
    }

    private fun sortAlphabetically(model: MutableList<AccountHolderContactViewModel>) {
        model.sortBy { it.name }
    }

    inner class MyChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                notifyItemChanged(selectedItem)
                selectedItem = layoutPosition
                notifyItemChanged(selectedItem)
                itemClickListener?.onItemClick(accountHolderContacts[selectedItem])
            }
        }

        var textView: TextView = itemView.tvChildItemTitle
        var imageCheck: ImageView = itemView.ivChecked
        var itemClickListener: OnItemClickListener<AccountHolderContactViewModel>? = null

        fun markAsChecked(check: Boolean) {
            imageCheck.visibility = if (check) View.VISIBLE else View.GONE
        }
    }


}



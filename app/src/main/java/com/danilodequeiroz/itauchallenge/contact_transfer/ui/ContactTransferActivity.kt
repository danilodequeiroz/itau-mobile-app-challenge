package com.danilodequeiroz.itauchallenge.contact_transfer.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.danilodequeiroz.itauchallenge.R
import com.danilodequeiroz.itauchallenge.common.MoneyUtils
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter.AccountBalancesAdapter
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter.AccountHolderContactsAdapter
import com.danilodequeiroz.itauchallenge.contact_transfer.ui.lists.adapter.OnItemClickListener
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountBalanceViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.list_item.AccountHolderContactViewModel
import com.danilodequeiroz.itauchallenge.viewmodel.screen.ContactTransferViewModel
import com.danilodequeiroz.restapi.model.OperationStatus
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.content_transfer.*
import javax.inject.Inject


class ContactTransferActivity : AppCompatActivity() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val transferViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ContactTransferViewModel::class.java)
    }

    private val bothListsAreVisible get() = rvSelectAccounts.visibility == View.VISIBLE && rvSelectContacts.visibility == View.VISIBLE
    private val onlyAccountListIsVisible get() = rvSelectAccounts.visibility == View.VISIBLE && rvSelectContacts.visibility == View.GONE
    private val onlyContactListIsVisible get() = rvSelectAccounts.visibility == View.GONE && rvSelectContacts.visibility == View.VISIBLE
    private val neitherListsAreVisible get() = rvSelectAccounts.visibility == View.GONE && rvSelectContacts.visibility == View.GONE

    private val contactAdapter by lazy { AccountHolderContactsAdapter(mutableListOf()) }
    private val accountBalanceAdapter by lazy { AccountBalancesAdapter(mutableListOf()) }
    private val confirmDialog by lazy { AlertDialog.Builder(this).create() }
    private val didSucceedDialog by lazy { AlertDialog.Builder(this).create() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_contact_transfer)
        setupToolbar()
        setupViewEventListeners()
        setupAdapter()
        observeData()
        savedInstanceState?.let {
            transferViewModel.restore()
        } ?: run {
            transferViewModel.initAccountBalances()
            transferViewModel.initAccountHolderContacts()
        }
    }


    private fun accountBalanceClickEvent() = object : OnItemClickListener<AccountBalanceViewModel> {
        override fun onItemClick(model: AccountBalanceViewModel) {
            tvSelectAccounts.text = String.format("%s (%s)", model.accountTypeName, model.balanceFormatted)
            updateConstraintsBasedOnAccountList()
        }
    }

    private fun accountHolderContactsEvent() = object : OnItemClickListener<AccountHolderContactViewModel> {
        override fun onItemClick(model: AccountHolderContactViewModel) {
            tvContactHeader.text = model.name
            updateConstraintsBasedOnContactList()
        }
    }

    private fun incrementMoneyValue(originalFormattedValue: String?, incrementBy: Int): String {
        return if (originalFormattedValue != null) {
            val formattedValueToAPI = MoneyUtils.formattedValueToAPI(originalFormattedValue)
            val cents = 100L
            val addedOne = formattedValueToAPI.plus(incrementBy * cents)
            MoneyUtils.formattedValue(addedOne)
        } else {
            getString(R.string.value_0_00)
        }
    }

    private fun observeData() {
        transferViewModel.stateAccountHolderContacts.observe(this, Observer {
            contactAdapter.putAllItems(it.data)
        })
        transferViewModel.stateAccountTypesBalances.observe(this, Observer {
            accountBalanceAdapter.putAllItems(it.data)
        })
        transferViewModel.moneyValue.observe(this, Observer {
            tvTransferMoneyValue.text = it
        })
        transferViewModel.stateOperationSuccessStatus.observe(this, Observer {
            when (it.data?.operationStatus) {
                OperationStatus.SUCCESS -> reBuildAndShowDidSucceedDialog(R.string.dlg_transfer_sucess)
                OperationStatus.HOLD -> reBuildAndShowDidSucceedDialog(R.string.dlg_transfer_hold)
                OperationStatus.BLOCKED -> reBuildAndShowDidSucceedDialog(R.string.dlg_transfer_blocked)
                OperationStatus.CANCELLED -> reBuildAndShowDidSucceedDialog(R.string.dlg_transfer_cancelled)
                OperationStatus.FALLBACK_ERROR -> reBuildAndShowDidSucceedDialog(R.string.dlg_transfer_fallback)
                else -> Unit
            }
            if(it.data?.operationStatus != null){
                refresh()
            }
        })
    }

    private fun setupAdapter() {
        rvSelectContacts.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ContactTransferActivity)
            adapter = contactAdapter
            contactAdapter.itemClickListener = accountHolderContactsEvent()
        }
        rvSelectAccounts.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ContactTransferActivity)
            adapter = accountBalanceAdapter
            accountBalanceAdapter.itemClickListener = accountBalanceClickEvent()
        }
    }

    private fun setupViewEventListeners() {
        llSelectContacts.setOnClickListener {
            updateConstraintsBasedOnContactList()
        }
        tvSelectAccounts.setOnClickListener {
            updateConstraintsBasedOnAccountList()
        }
        btAddOneBRL.setOnClickListener {
            transferViewModel.moneyValue.value =
                incrementMoneyValue(transferViewModel.moneyValue.value, 1)
        }

        btAddFiveBRL.setOnClickListener {
            transferViewModel.moneyValue.value =
                incrementMoneyValue(transferViewModel.moneyValue.value, 5)
        }

        btAddTenBRL.setOnClickListener {
            transferViewModel.moneyValue.value =
                incrementMoneyValue(transferViewModel.moneyValue.value, 10)
        }
        btConfirm.setOnClickListener {
            if (validateInputs()) {
                reBuildAndShowConfirmDialog()
            } else {
                Toast.makeText(this, R.string.validation_error, Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun reBuildAndShowConfirmDialog() {
        val (moneyValue, contactName, accountType) = listOf(
            transferViewModel.moneyValue.value,
            contactAdapter.accountHolderContacts[contactAdapter.selectedItem].name,
            accountBalanceAdapter.accountsBalances[accountBalanceAdapter.selectedItem].accountTypeName
        )
        val title = getString(R.string.dlg_confirm_1, moneyValue, contactName, accountType?.toLowerCase())

        confirmDialog.setTitle(R.string.lbl_transfer)
        confirmDialog.setMessage(title)
        confirmDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.lbl_confirm)) { dialog, _ ->
            dialog.dismiss()
            transferViewModel.postAccountHolderContactTransfer()
        }
        confirmDialog.setButton(
            DialogInterface.BUTTON_NEGATIVE,
            getString(R.string.lbl_cancel)
        ) { dialog, _ -> dialog.dismiss() }
        confirmDialog.show()
    }

    private fun reBuildAndShowDidSucceedDialog(@StringRes message: Int) {
        didSucceedDialog.setTitle(R.string.lbl_transfer)
        didSucceedDialog.setMessage(getString(message))
        didSucceedDialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            getString(R.string.lbl_ok)
        ) { dialog, _ -> dialog.dismiss() }
        didSucceedDialog.show()
    }

    fun refresh(){
        tvSelectAccounts.text = getString(R.string.lbl_accounts)
        tvContactHeader.text = getString(R.string.lbl_contacts)
        accountBalanceAdapter.clear()
        contactAdapter.clear()
        transferViewModel.moneyValue.value = "R$ 0,00"
        transferViewModel.initAccountBalances()
        transferViewModel.initAccountHolderContacts()
    }

    fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val mTitle = toolbar.findViewById(R.id.toolbar_title) as TextView

        setSupportActionBar(toolbar)
        mTitle.text = toolbar.title

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun updateConstraintsBasedOnContactList() {
        when {
            bothListsAreVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_account_only)
            onlyAccountListIsVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_both)
            onlyContactListIsVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer)
            neitherListsAreVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_contact_only)
        }
    }

    fun updateConstraintsBasedOnAccountList() {
        when {
            bothListsAreVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_contact_only)
            onlyAccountListIsVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer)
            onlyContactListIsVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_both)
            neitherListsAreVisible -> updateConstraintsByLayoutRes(R.layout.content_transfer_account_only)
        }
    }

    fun updateConstraintsByLayoutRes(@LayoutRes id: Int) {
        val newConstraintSet = ConstraintSet()
        val sv = layoutInflater.inflate(id, null) as NestedScrollView
        newConstraintSet.clone(sv.findViewById<ConstraintLayout>(R.id.constraintContactTransferRoot))
        newConstraintSet.applyTo(constraintContactTransferRoot)
        val transition = AutoTransition()
        transition.interpolator = OvershootInterpolator()
        TransitionManager.beginDelayedTransition(constraintContactTransferRoot)
    }

    private fun validateInputs(): Boolean {
        return (contactAdapter.selectedItem >= 0 &&
                accountBalanceAdapter.selectedItem >= 0 &&
                transferViewModel.moneyValue.value != getString(R.string.value_0_00))
    }

}

package com.danilodequeiroz.restapi.di

import com.google.gson.GsonBuilder
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


fun providesRetrofitClient(): Retrofit {
    val okHttpClientBuilder = OkHttpClient.Builder()

    okHttpClientBuilder.addInterceptor(OkHttpProfilerInterceptor())
    okHttpClientBuilder.addInterceptor(HttpLoggingInterceptor().apply { this.level = HttpLoggingInterceptor.Level.BODY })

    return Retrofit.Builder()
        .client(okHttpClientBuilder.build())
        .baseUrl("http://localhost:8086/")
        .addConverterFactory(providesGsonConverter())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

private fun providesGsonConverter(): GsonConverterFactory {
    val builder = GsonBuilder()
    return GsonConverterFactory.create(builder.create())
}
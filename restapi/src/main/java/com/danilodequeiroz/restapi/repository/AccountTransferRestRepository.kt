package com.danilodequeiroz.restapi.repository

import com.danilodequeiroz.restapi.model.AccountBalance
import com.danilodequeiroz.restapi.model.AccountHolderContact
import com.danilodequeiroz.restapi.model.TransferOperationStatus
import io.reactivex.Single

interface AccountTransferRestRepository {
    fun getAccountHolderContacts(): Single<MutableList<AccountHolderContact>>

    fun getAccountTypesBalances(): Single<MutableList<AccountBalance>>

    fun postAccountHolderContactTransfer(): Single<TransferOperationStatus>
}
package com.danilodequeiroz.restapi.repository.real

import com.danilodequeiroz.restapi.model.AccountBalance
import com.danilodequeiroz.restapi.model.AccountHolderContact
import com.danilodequeiroz.restapi.model.TransferOperationStatus
import com.danilodequeiroz.restapi.repository.AccountTransferRestRepository
import com.danilodequeiroz.restapi.service.AccountTransferService
import io.reactivex.Single

class AccountTransferRestRepositoryImpl
    (val accountTransferService: AccountTransferService) : AccountTransferRestRepository {

    override fun getAccountHolderContacts(): Single<MutableList<AccountHolderContact>> =
        accountTransferService.getAccountHolderContacts()


    override fun getAccountTypesBalances(): Single<MutableList<AccountBalance>> =
        accountTransferService.getAccountTypesBalances()


    override fun postAccountHolderContactTransfer(): Single<TransferOperationStatus> =
        accountTransferService.postAccountHolderContactTransfer()

}
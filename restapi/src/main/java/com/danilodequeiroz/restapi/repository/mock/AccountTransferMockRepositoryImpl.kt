package com.danilodequeiroz.restapi.repository.mock

import com.danilodequeiroz.restapi.model.AccountBalance
import com.danilodequeiroz.restapi.model.AccountHolderContact
import com.danilodequeiroz.restapi.repository.AccountTransferRestRepository
import io.reactivex.Single

class AccountTransferMockRepositoryImpl  : AccountTransferRestRepository {

    override fun getAccountHolderContacts(): Single<MutableList<AccountHolderContact>> =
        Single.just(MockLists().mockAccountHolderContacts)

    override fun getAccountTypesBalances(): Single<MutableList<AccountBalance>> =
        Single.just(MockLists().mockAccountTypesBalances)

    override fun postAccountHolderContactTransfer() =
        Single.just(MockOperationSuccessStatus().successStatus)

}
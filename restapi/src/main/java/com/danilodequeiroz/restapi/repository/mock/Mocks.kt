package com.danilodequeiroz.restapi.repository.mock

import com.danilodequeiroz.restapi.model.*
import java.util.*

class MockLists {

    val savingBalance = 3000000L
    val transactionBalance = 450000L

    val mockAccountTypesBalances = mutableListOf(
        AccountBalance(UUID.randomUUID().toString(), AccountType.TRANSACTION_ACCOUNT, transactionBalance),
        AccountBalance(UUID.randomUUID().toString(), AccountType.SAVING_ACCOUNT, savingBalance)
    )

    val mockAccountHolderContacts = mutableListOf(
        AccountHolderContact(UUID.randomUUID().toString(), "Corey Taylor"),
        AccountHolderContact(UUID.randomUUID().toString(), "Mick Thomson"),
        AccountHolderContact(UUID.randomUUID().toString(), "Shawn Crahan"),
        AccountHolderContact(UUID.randomUUID().toString(), "Craig Jones"),
        AccountHolderContact(UUID.randomUUID().toString(), "Sid Wilson"),
        AccountHolderContact(UUID.randomUUID().toString(), "James Root"),
        AccountHolderContact(UUID.randomUUID().toString(), "Jay Weinberg"),
        AccountHolderContact(UUID.randomUUID().toString(), "Alessandro Venturella")
    )
}

class MockOperationSuccessStatus {

    val successStatus = TransferOperationStatus(OperationStatus.SUCCESS)
    val blockedStatus = TransferOperationStatus(OperationStatus.BLOCKED)
    val holdStatus = TransferOperationStatus(OperationStatus.HOLD)
    val cancelledStatus = TransferOperationStatus(OperationStatus.CANCELLED)

}
package com.danilodequeiroz.restapi.service

import com.danilodequeiroz.restapi.model.AccountBalance
import com.danilodequeiroz.restapi.model.AccountHolderContact
import com.danilodequeiroz.restapi.model.TransferOperationStatus
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST

interface AccountTransferService{

    @GET("accountholder/contacts")
    fun getAccountHolderContacts(): Single<MutableList<AccountHolderContact>>

    @GET("account/types/balance")
    fun getAccountTypesBalances(): Single<MutableList<AccountBalance>>

    @POST("process/transfer")
    fun postAccountHolderContactTransfer(): Single<TransferOperationStatus>
}

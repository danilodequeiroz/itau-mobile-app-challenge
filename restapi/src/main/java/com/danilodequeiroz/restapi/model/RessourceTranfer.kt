package com.danilodequeiroz.restapi.model

data class TransferOperationStatus(val operationStatus: OperationStatus)

package com.danilodequeiroz.restapi.model

data class AccountBalance(val uuid:String, val accountType: AccountType, val balance: Long)

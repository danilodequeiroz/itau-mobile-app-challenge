package com.danilodequeiroz.restapi.model

data class AccountHolderContact(val uuid:String, val contactName:String)
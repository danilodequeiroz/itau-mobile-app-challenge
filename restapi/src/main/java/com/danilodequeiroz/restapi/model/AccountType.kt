package com.danilodequeiroz.restapi.model

enum class AccountType {
    SAVING_ACCOUNT, TRANSACTION_ACCOUNT
}
package com.danilodequeiroz.restapi.model

enum class OperationStatus {
    SUCCESS, HOLD, BLOCKED, CANCELLED, FALLBACK_ERROR
}